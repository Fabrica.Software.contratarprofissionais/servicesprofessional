package br.com.services.api.services;

import br.com.services.api.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User get(long id);

    void persist(User user);

    void delete(User user);
}
